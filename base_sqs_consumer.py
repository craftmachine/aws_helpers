import json
import logging
import os
import sys
import threading
from uuid import uuid4

import boto3

from aws_helpers.push_cloudwatch_metric import push_cloudwatch_metric


class BaseMessage():
    '''Base class for message'''

    def __init__(self):
        self.uid = uuid4()


class BaseSQSConsumer():

    def __init__(self, queue_url: str, aws_access_key_id: str, aws_secret_access_key: str,
                 region_name: str, function_name: str, pipeline_name: str,
                 wait_time: int=5, max_number_of_messages: int=10, *args, **kwargs):
        self.pipeline_name = pipeline_name
        self.function_name = function_name
        self._region_name = region_name
        self._aws_access_key_id = aws_access_key_id
        self._aws_secret_access_key = aws_secret_access_key
        self._client = boto3.client('sqs', region_name=region_name,
                                    aws_access_key_id=aws_access_key_id,
                                    aws_secret_access_key=aws_secret_access_key)
        self._queue_url = queue_url
        self._wait_time = wait_time
        self._max_number_of_messages = max_number_of_messages
        self._cloudwatch_client_thread = threading.local()
        self.LOGGER = logging.getLogger()
        self._configure_logger()

    def run(self):
        self.LOGGER.info(f"Start consuming queue {self._queue_url}")
        while True:
            self._recieve_messages()

    def _recieve_messages(self):
        sqs_response = self._client.receive_message(
            QueueUrl=self._queue_url,
            WaitTimeSeconds=self._wait_time,
            MaxNumberOfMessages=self._max_number_of_messages
        )
        for sqs_message in sqs_response.get('Messages', []):
            self.LOGGER.info(f"[SQS] Message recieved: {sqs_message}")
            receipt_handle = sqs_message['ReceiptHandle']
            message_body = json.loads(sqs_message['Body'])
            message = self._build_message(message_body)
            try:
                self._client.delete_message(
                    QueueUrl=self._queue_url,
                    ReceiptHandle=receipt_handle
                )
                self._handle_message(message)
            except Exception:
                self._push_cloudwatch_metric('execution_error', message)
                self.LOGGER.exception(f'[{message.uid}] Exception with message: {message_body}')

    def _handle_message(self, message):
        # Implement this instead of your lambda function
        raise NotImplementedError

    def _build_message(self, message_body) -> BaseMessage:
        # Build your message instance here
        raise NotImplementedError

    def _push_cloudwatch_metric(self, metric_name: str, message: BaseMessage, value: int=1):
        if not hasattr(self._cloudwatch_client_thread, 'cloudwatch_client'):
            self._cloudwatch_client_thread.cloudwatch_client = boto3.client(
                'cloudwatch', region_name=self._region_name,
                aws_access_key_id=self._aws_access_key_id,
                aws_secret_access_key=self._aws_secret_access_key)
        push_cloudwatch_metric(self._cloudwatch_client_thread.cloudwatch_client, metric_name,
                               self.function_name, self.pipeline_name, value)

    def _configure_logger(self):
        self.LOGGER.setLevel(logging.INFO)
        handler = logging.StreamHandler(sys.stdout)
        formatter = logging.Formatter('%(asctime)s [%(levelname)s]%(message)s')
        handler.setFormatter(formatter)
        self.LOGGER.addHandler(handler)
        logging.getLogger('boto3').setLevel(logging.WARNING)
        logging.getLogger('botocore').setLevel(logging.WARNING)


if __name__ == "__main__":
    queue_url = os.environ['SQS_QUEUE_URL']
    aws_access_key_id = os.environ['AWS_ACCESS_KEY_ID']
    aws_secret_access_key = os.environ['AWS_SECRET_ACCESS_KEY']
    region_name = os.environ.get('AWS_REGION', 'us-east-1')
    pipeline_name = os.environ['PIPELINE_NAME']
    function_name = os.environ['FUNCTION_NAME']
    wait_time = 5
    max_number_of_messages = 10
    a = BaseSQSConsumer(queue_url, aws_access_key_id, aws_secret_access_key,
                        region_name, function_name, pipeline_name, wait_time,
                        max_number_of_messages)
    a.run()
