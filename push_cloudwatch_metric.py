def push_cloudwatch_metric(client, metric_name: str, function_name: str,
                           pipeline_name: str, value: int=1, namespace: str='data_pipelines'):
    client.put_metric_data(
        Namespace=namespace,
        MetricData=[
            {
                'MetricName': metric_name,
                'Dimensions': [
                    {
                        'Name': 'pipeline_name',
                        'Value': pipeline_name
                    },
                    {
                        'Name': 'function_name',
                        'Value': function_name
                    }
                ],
                'Value': value,
            },
        ]
    )
