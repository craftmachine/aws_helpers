import logging
import os
import sys


def set_function_env(_file_):
    def decorator(func):
        file_path = os.path.join(os.getcwd(), os.path.expanduser(_file_))
        script_dir = os.path.dirname(os.path.realpath(file_path))
        sys.path.append(os.path.normpath(os.path.join(script_dir, './src/')))
        return func
    return decorator


def set_environ(variables):
    for key, value in variables.items():
        os.environ[key] = value
