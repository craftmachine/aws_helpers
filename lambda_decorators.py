import base64
import json
import logging

import boto3

from aws_helpers import message_bus

RETRY_COUNT = 3
FAULT_RESULT = {"status": message_bus.STATUS_FAIL}


def apply_retry_logic(environment, retry_count=RETRY_COUNT):
    def decorator(lambda_function):
        def wrapper(event, context):
            repeat_messages = []
            try:
                result = lambda_function(event, context)
            except Exception:
                logging.warning('Lambda failed', exc_info=True)
                result = FAULT_RESULT

                stream_service, channel_name = _get_stream_parameters(
                    event, environment)
                for record in event['Records']:
                    stream_record = record[stream_service]
                    message = _resolve_record_retry(
                        stream_service, stream_record, retry_count)
                    if message:
                        repeat_messages.append(message)
                    else:
                        logging.error(
                            "Event record maximum retry count exceeded")

                if repeat_messages:
                    _put_event_messages(
                        stream_service, channel_name, repeat_messages)
            return result
        return wrapper
    return decorator


def _get_stream_parameters(event, environment):
    record = event['Records'][0]
    if record.get('kinesis'):
        stream_service = 'kinesis'
        channel_name = environment['kinesis_event_source_arn'].split('/').pop()
    elif record.get('Sns'):
        stream_service = 'Sns'
        channel_name = environment['sns_event_source_arn'].split(':').pop()
    else:
        raise Exception('Not supported event source')
    return stream_service, channel_name


def _resolve_record_retry(stream_service, stream_record, retry_count):
    if stream_service == 'kinesis':
        record_data = json.loads(base64.b64decode(stream_record['data']))
        retry_attempt = record_data.get('retry_attempt', 1)
        record_data['retry_attempt'] = retry_attempt + 1
        partition_key = stream_record['partitionKey']
        message = {"Data": json.dumps(
            record_data), "PartitionKey": partition_key}
    elif stream_service == 'Sns':
        message = stream_record['Message']
        record_data = message if isinstance(
            message, dict) else json.loads(message)
        retry_attempt = record_data.get('retry_attempt', 1)
        record_data['retry_attempt'] = retry_attempt + 1
        message = record_data

    if retry_attempt <= retry_count:
        return message


def _put_event_messages(stream_service, channel_name, messages):
    client = boto3.client(stream_service.lower())
    if stream_service == 'kinesis':
        message_bus.put_message_batch_to_kinesis(
            client, channel_name, messages)
    elif stream_service == 'Sns':
        message_bus.put_messages_to_sns(client, channel_name, messages)
