import json
import pickle
import sys
from itertools import islice
from typing import Iterator


def get_chunks(iterable: Iterator, size: int) -> Iterator:
    i = iter(iterable)
    chunk = list(islice(i, size))
    while chunk:
        yield chunk
        chunk = list(islice(i, size))


def get_size(obj, of_json=True) -> int:
    if of_json:
        obj_dump = json.dumps(obj)
    else:
        obj_dump = pickle.dumps(obj)
    return sys.getsizeof(obj_dump)
