import base64
import concurrent.futures
import json
import logging
import sys
import time
from typing import Iterator, List

from botocore.errorfactory import ClientError

from aws_helpers.lib import get_chunks, get_size

STATUS_OK = 'OK'
STATUS_FAIL = 'Fail'

SQS_BATCH_SIZE = 10
SNS_MESSAGE_MAX_SIZE = 256 * 1024                   # 256 Kb
SQS_MESSAGE_MAX_SIZE = 256 * 1024                   # 256 Kb
KINESIS_RECORD_MAX_SIZE = 1024 * 1024               # 1 Mb
KINESIS_REQUEST_MAX_SIZE = 5 * 1024 * 1024          # 5 Mb
KINESIS_REQUEST_MAX_SIZE_PER_SEC = 1024 * 1024      # 1 Mb per second
KINESIS_RECORDS_MAX_COUNT = 500


EMPTY_RECORD_BATCH_LENGTH = 2                       # len(json.dumps([]))
SERIALIZATION_DELIMITER_SIZE = 2                    # '[1, 2]', comma and space

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def put_message(client, channel_name: str, data: dict, meta: dict={}):
    LOGGER.warn("DEPRECATED METHOD put_message, please use put_messages")
    return put_messages(client, channel_name, [data], meta)


def put_messages(client, channel_name: str, data: List[dict], meta: dict={}):
    messages = [{"data": datum, "meta": meta} for datum in data]
    if client._endpoint._endpoint_prefix == 'kinesis':
        return _put_messages_to_kinesis(client, channel_name, messages)
    if client._endpoint._endpoint_prefix == 'sns':
        return put_messages_to_sns(client, channel_name, messages)
    else:
        raise RuntimeError("Unknown client type '{}'".format(type(client)))


def _put_message_batch_to_sqs(sqs_client, queue_url: str, message_batch: List, message_id_offset: int):
    response = sqs_client.send_message_batch(
        QueueUrl=queue_url,
        Entries=[{'Id': str(message_id_offset + i), 'MessageBody': json.dumps(msg)}
                 for i, msg in enumerate(message_batch)]
    )

    for send_message_error in response.get('Failed', []):
        message_id = int(send_message_error['Id'])
        message = message_batch[message_id - message_id_offset]
        LOGGER.error(f"[SQS] Error pushing message, "
                     f"size {sys.getsizeof(message)}: "
                     f"{message}, "
                     f"SQS response: {send_message_error}")

    return {
        'OK': len(response.get('Successful', [])),
        'Errors': len(response.get('Failed', []))
    }


def _put_messages_to_sqs_multithreaded(sqs_client, queue_url: str, messages: Iterator, max_threads=10):
    success_count = 0
    fail_count = 0

    with concurrent.futures.ThreadPoolExecutor(max_workers=max_threads) as executor:
        for res in executor.map(
                lambda chunk_tuple: _put_message_batch_to_sqs(sqs_client, queue_url, chunk_tuple[1], chunk_tuple[0]),
                enumerate(get_chunks(messages, SQS_BATCH_SIZE))):
            success_count += res['OK']
            fail_count += res['Errors']

    return {
        'OK': success_count,
        'Errors': fail_count
    }


def put_messages_to_sqs(sqs_client, queue_url: str, messages: Iterator, multithreaded=False, max_threads=None):
    success_count = 0
    fail_count = 0

    if multithreaded:
        if max_threads is not None:
            return _put_messages_to_sqs_multithreaded(sqs_client, queue_url, messages, max_threads)
        else:
            return _put_messages_to_sqs_multithreaded(sqs_client, queue_url, messages)
    else:
        for i, message_batch in enumerate(get_chunks(messages, SQS_BATCH_SIZE)):
            res = _put_message_batch_to_sqs(sqs_client, queue_url, message_batch, i)
            success_count += res['OK']
            fail_count += res['Errors']

    return {
        'OK': success_count,
        'Errors': fail_count
    }


def decode_event(event: dict):
    return [_decode_record(record) for record in event['Records']]


def _decode_record(record):
    if record.get('kinesis'):
        return json.loads(base64.b64decode(record["kinesis"]["data"]))
    elif record.get('Sns'):
        message = json.loads(record['Sns']["Message"])
        if type(message) is dict:
            return message
        else:
            return [json.loads(msg) for msg in message]
    elif record.get('eventSource') == 'aws:sqs':
        return json.loads(record['body'])
    else:
        raise RuntimeError("Unknown event type:\n{}".format(record))


def _put_messages_to_kinesis(client, channel_name, messages):
    LOGGER.info(f'[KINESIS] Start to put {len(messages)} messages')
    batch_responses = []
    errors_count = 0
    status = STATUS_OK
    records_batch = []
    dumped_records_batch_length = EMPTY_RECORD_BATCH_LENGTH
    for msg in messages:
        partition_key = msg['data'].pop("PartitionKey", str(msg))[:256]
        record = {"Data": json.dumps(msg),
                  "PartitionKey": partition_key}
        record_string = json.dumps(record)
        dumped_message_size = len(record_string) + SERIALIZATION_DELIMITER_SIZE
        if dumped_message_size > KINESIS_RECORD_MAX_SIZE:
            error_message = "Record {} exceeds Kinesis record max size {}"
            raise RuntimeError(error_message.format(record, KINESIS_RECORD_MAX_SIZE))

        future_dumped_message_size = dumped_message_size + dumped_records_batch_length
        request_size_exceed = future_dumped_message_size >= KINESIS_REQUEST_MAX_SIZE_PER_SEC
        records_count_exceed = len(records_batch) + 1 >= KINESIS_RECORDS_MAX_COUNT

        if request_size_exceed or records_count_exceed:
            response = put_message_batch_to_kinesis(client, channel_name, records_batch)
            batch_responses.append(response)

            records_batch = [record]
            dumped_records_batch_length = EMPTY_RECORD_BATCH_LENGTH + dumped_message_size
        else:
            records_batch.append(record)
            dumped_records_batch_length += dumped_message_size
    if len(records_batch) > 0:
        response = put_message_batch_to_kinesis(client, channel_name, records_batch)
        batch_responses.append(response)

    if STATUS_FAIL in [resp["status"] for resp in batch_responses]:
        status = STATUS_FAIL
        errors_count = sum([resp["errors_count"] for resp in batch_responses])
    return {"status": status, "errors_count": errors_count}


def put_message_batch_to_kinesis(client, channel_name, batch):
    status = STATUS_OK
    errors_count = 0
    LOGGER.info(f'[KINESIS] Put message. Size: {get_size(batch, of_json=False)}')
    try:
        start_time = time.time()
        response = client.put_records(StreamName=channel_name, Records=batch)
        delta = time.time() - start_time
        if delta <= 1:
            time.sleep(1 - delta)
    except ClientError as e:
        LOGGER.exception('[KINESIS]')
        return {"status": STATUS_FAIL, "errors_count": len(batch)}
    if response['FailedRecordCount'] > 0:
        LOGGER.warn(f"Kinesis failed to put some messages:\n{response}")
        status = STATUS_FAIL
        errors_count = response['FailedRecordCount']
    return {"status": status, "errors_count": errors_count}


def put_messages_to_sns(client, channel_name, messages):
    LOGGER.info(f'[SNS] Start to put {len(messages)} messages')
    topic = client.create_topic(Name=channel_name)
    status = STATUS_OK
    errors_count = 0
    for message in messages:
        message_string = json.dumps(message)
        LOGGER.info(f'[SNS] Put message. Size: {sys.getsizeof(message_string)}')
        try:
            response = client.publish(TopicArn=topic['TopicArn'], Message=message_string)
        except ClientError:
            LOGGER.exception('[SNS]')
            return {"status": STATUS_FAIL, "errors_count": len(messages)}
        if not response.get('MessageId'):
            LOGGER.warn(f"Bad SNS response: {response}")
            status = STATUS_FAIL
            errors_count += len(messages)
    return {"status": status, "errors_count": errors_count}
