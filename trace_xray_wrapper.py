from aws_xray_sdk.core import patch_all, xray_recorder

patch_all()


def trace_xray(function, *args, **kwargs):
    ray_name = "## {}".format(function.__name__)
    decorated_function = xray_recorder.capture(ray_name)(function)
    ans = decorated_function(*args, **kwargs)
    return ans
