import logging

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def get_session(db_user, db_password, db_host, db_port, db_name):
    LOGGER.info(f"[SQLALCHEMY] Engine for {db_name}")
    kwargs = {'pool_size': 1}
    sqlalchemy_version = float('.'.join(sqlalchemy.__version__.split('.')[:2]))
    if sqlalchemy_version > 1.1:
        kwargs.update({'pool_pre_ping': True})
    engine = create_engine("postgresql://{0}:{1}@{2}:{3}/{4}".format(
        db_user, db_password, db_host, db_port, db_name), **kwargs
    )
    LOGGER.info(f"[SQLALCHEMY] Session for {db_name}")
    return scoped_session(sessionmaker(autocommit=False, bind=engine))
