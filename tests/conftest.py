import base64
import json

import boto3
import pytest
from moto import mock_cloudwatch, mock_sqs

KINESIS_EVENT_DATA = """
eyJkYXRhIjogeyJrZXkxIjogInZhbHVlMSIsICJrZXkyIjogInZhbHVlMiJ9LCAibWV0YSI6IHsibWV0
YWtleTEiOiAibWV0YXZhbHVlMSIsICJtZXRha2V5MiI6ICJtZXRhdmFsdWUyIn19
""".replace('\n', '').replace('\r', '')

KINESIS_EVENT = '''
{
  "Records": [
    {
      "eventID": "shardId-000000000000:49545115243490985018280067714973144582180062593244200961",
      "eventVersion": "1.0",
      "kinesis": {
        "partitionKey": "partitionKey-3",
        "data": "data",
        "kinesisSchemaVersion": "1.0",
        "sequenceNumber": "49545115243490985018280067714973144582180062593244200961"
      },
      "invokeIdentityArn": "identityarn",
      "eventName": "aws:kinesis:record",
      "eventSourceARN": "eventsourcearn",
      "eventSource": "aws:kinesis",
      "awsRegion": "us-east-1"
    }
  ]
}'''


SNS_EVENT = '''
{
  "Records": [
    {
      "EventVersion": "1.0",
      "EventSubscriptionArn": "eventsubscriptionarn",
      "EventSource": "aws:sns",
      "Sns": {
        "SignatureVersion": "1",
        "Timestamp": "1970-01-01T00:00:00.000Z",
        "Signature": "EXAMPLE",
        "SigningCertUrl": "EXAMPLE",
        "MessageId": "95df01b4-ee98-5cb9-9903-4c221d41eb5e",
        "Message": "Where will be serialized JSON with message",
        "MessageAttributes": {
          "Test": {
            "Type": "String",
            "Value": "TestString"
          },
          "TestBinary": {
            "Type": "Binary",
            "Value": "TestBinary"
          }
        },
        "Type": "Notification",
        "UnsubscribeUrl": "EXAMPLE",
        "TopicArn": "topicarn",
        "Subject": "TestInvoke"
      }
    }
  ]
}'''

KINESIS_STREAM = 'kinesis-stream'

SQS_EVENT = '''
{
  "Records": [
    {
      "messageId": "",
      "receiptHandle": "",
      "body": "[{'key': 'value'}]",
      "attributes": {
        "ApproximateReceiveCount": "",
        "SentTimestamp": "",
        "SenderId": "",
        "ApproximateFirstReceiveTimestamp": ""
      },
      "messageAttributes": {},
      "md5OfBody": "",
      "eventSource": "aws:sqs",
      "eventSourceARN": "",
      "awsRegion": ""
    }
  ]
}
'''


@pytest.fixture()
def kinesis_event():
    event = json.loads(KINESIS_EVENT)
    event['Records'][0]['kinesis']['data'] = KINESIS_EVENT_DATA
    return event


@pytest.fixture()
def sns_event(event_data, event_meta):
    event = json.loads(SNS_EVENT)
    message = {"data": event_data, "meta": event_meta}
    event['Records'][0]['Sns']['Message'] = json.dumps(message)
    return event


@pytest.fixture()
def sqs_event(event_data, event_meta):
    event = json.loads(SQS_EVENT)
    message = {"data": event_data, "meta": event_meta}
    event['Records'][0]['body'] = json.dumps(message)
    return event


@pytest.fixture()
def event_data():
    return {"key1": "value1", "key2": "value2"}


@pytest.fixture()
def event_meta():
    return {"metakey1": "metavalue1", "metakey2": "metavalue2"}


@pytest.fixture()
def access_key():
    return 'access_key'


@pytest.fixture()
def secret_key():
    return 'secret_key'


@pytest.fixture()
def region():
    return 'us-east-1'


@pytest.yield_fixture()
def sqs(access_key, secret_key, region):
    with mock_sqs():
        yield boto3.client('sqs', region_name=region, aws_access_key_id=access_key,
                           aws_secret_access_key=secret_key)


@pytest.fixture()
def queue(sqs):
    return sqs.create_queue(QueueName='test')


@pytest.yield_fixture()
def cloudwatch(access_key, secret_key, region):
    with mock_cloudwatch():
        yield boto3.client('cloudwatch', region_name=region, aws_access_key_id=access_key,
                           aws_secret_access_key=secret_key)
