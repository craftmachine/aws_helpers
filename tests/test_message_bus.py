import base64
import json
import logging

import boto3
import botocore.config
import pytest
from moto import mock_kinesis, mock_sns, mock_sqs, mock_xray

from message_bus import (KINESIS_RECORD_MAX_SIZE, KINESIS_RECORDS_MAX_COUNT,
                         SQS_MESSAGE_MAX_SIZE, decode_event, put_messages,
                         put_messages_to_sqs)

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)
KINESIS_STREAM = 'kinesis-stream'
SNS_TOPIC = 'sns-topic'


@mock_kinesis
def test_put_messages_to_kinesis(event_data, event_meta):
    kinesis = boto3.client('kinesis', region_name='us-east-1')
    kinesis.create_stream(StreamName=KINESIS_STREAM, ShardCount=1)
    result = put_messages(kinesis, KINESIS_STREAM, [event_data], event_meta)
    assert result.get('status') == 'OK'
    records = _get_all_records_in_kinesis(kinesis, KINESIS_STREAM)
    kinesis_record = json.loads(records[0]['Data'].decode('utf-8'))
    assert type(kinesis_record) == dict
    assert kinesis_record == {"data": event_data, "meta": event_meta}


@mock_kinesis
def test_put_many_messages_to_kinesis(event_data, event_meta):
    kinesis = boto3.client('kinesis', region_name='us-east-1')
    kinesis.create_stream(StreamName=KINESIS_STREAM, ShardCount=1)
    data = [event_data] * (KINESIS_RECORDS_MAX_COUNT + 1)
    result = put_messages(kinesis, KINESIS_STREAM, data, event_meta)
    assert result.get('status') == 'OK'
    records = _get_all_records_in_kinesis(kinesis, KINESIS_STREAM)
    assert len(records) == len(data)


@mock_kinesis
def test_put_large_message_to_kinesis(event_data, event_meta):
    kinesis = boto3.client('kinesis', region_name='us-east-1')
    kinesis.create_stream(StreamName=KINESIS_STREAM, ShardCount=1)
    large_string = 'a' * KINESIS_RECORD_MAX_SIZE
    event_data['large_key'] = large_string
    with pytest.raises(RuntimeError) as excinfo:
        result = put_messages(kinesis, KINESIS_STREAM, [event_data], event_meta)
        assert result.get('status') == 'Fail'
    assert 'record max size' in str(excinfo.value)


@mock_sqs
def test_put_messages_to_sqs():
    messages = range(100)
    sqs = boto3.client('sqs', region_name='us-east-1')
    queue = sqs.create_queue(QueueName='test')
    result = put_messages_to_sqs(sqs, queue['QueueUrl'], messages)
    assert result['OK'] == len(messages)
    message_count = sqs.get_queue_attributes(
        QueueUrl=queue['QueueUrl'],
        AttributeNames=['ApproximateNumberOfMessages']
    )['Attributes']['ApproximateNumberOfMessages']
    assert int(message_count) == len(messages)


@mock_sqs
def test_put_messages_to_sqs_multithreaded():
    messages = range(1000)
    max_threads = 10
    cfg = botocore.config.Config(max_pool_connections=max_threads)
    sqs = boto3.client('sqs', region_name='us-east-1', config=cfg)
    queue = sqs.create_queue(QueueName='test')
    result = put_messages_to_sqs(sqs, queue['QueueUrl'], messages, multithreaded=True, max_threads=max_threads)
    assert result['OK'] == len(messages)
    message_count = sqs.get_queue_attributes(
        QueueUrl=queue['QueueUrl'],
        AttributeNames=['ApproximateNumberOfMessages']
    )['Attributes']['ApproximateNumberOfMessages']
    assert int(message_count) == len(messages)


@mock_sns
@mock_sqs
def test_put_messages_to_sns():
    sns = boto3.client('sns', region_name='us-east-1')
    topic = sns.create_topic(Name="some-topic")
    sqs = boto3.client('sqs', region_name='us-east-1')
    queue = sqs.create_queue(QueueName="test-queue")

    sns.subscribe(
        TopicArn=topic['TopicArn'],
        Protocol='sqs',
        Endpoint="arn:aws:sqs:us-east-1:123456789012:test-queue"
    )
    result = put_messages(sns, 'some-topic', "123")
    assert result.get('errors_count') == 0


def test_decode_kinesis_event(kinesis_event, event_data, event_meta):
    expected_message = [{"data": event_data, "meta": event_meta}]
    actual_message = decode_event(kinesis_event)
    assert expected_message == actual_message


def test_decode_sns_event(sns_event, event_data, event_meta):
    expected_message = {"data": event_data, "meta": event_meta}
    actual_message = decode_event(sns_event)[0]
    assert type(expected_message) == type(actual_message)
    assert expected_message == actual_message


def test_decode_sqs_event(sqs_event, event_data, event_meta):
    expected_message = {"data": event_data, "meta": event_meta}
    actual_message = decode_event(sqs_event)[0]
    assert type(expected_message) == type(actual_message)
    assert expected_message == actual_message


def _get_all_records_in_kinesis(client, stream_name):
    iterator_query_dict = {
        'StreamName': stream_name,
        'ShardIteratorType': 'TRIM_HORIZON',
        'ShardId': 'shardId-000000000000'
    }
    iterator = client.get_shard_iterator(
        **iterator_query_dict)['ShardIterator']
    return client.get_records(ShardIterator=iterator)['Records']
