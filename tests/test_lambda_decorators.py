import json
import logging
import os
from base64 import b64encode

import boto3
import pytest
from moto import mock_kinesis, mock_sns, mock_sqs

from aws_helpers.lambda_decorators import (FAULT_RESULT, RETRY_COUNT,
                                           _get_stream_parameters,
                                           _put_event_messages,
                                           _resolve_record_retry,
                                           apply_retry_logic)

os.environ['AWS_DEFAULT_REGION'] = 'eu-central-1'

LAMBDA_RESULT = 'result'
CHANNEL_NAME = 'channel-name'
KINESIS_ARN = f'arn:aws:kinesis:region:account_id:stream/{CHANNEL_NAME}'
SNS_ARN = f'arn:aws:sns:region:account_id:{CHANNEL_NAME}'


def normal_lambda_function(event, context): return LAMBDA_RESULT


def faulty_lambda_function(event, context): raise Exception


def test_apply_retry_logic_on_normal_lambda_function():
    environment = {}
    event = {}
    context = {}

    result = apply_retry_logic(environment)(
        normal_lambda_function)(event, context)
    assert result == LAMBDA_RESULT


@mock_sns
@mock_sqs
@mock_kinesis
@pytest.mark.parametrize('record', [
    {'Sns': {'Message': {}}},
    {'kinesis': {'data': b64encode(
        json.dumps({'data': {}}).encode()).decode(), 'partitionKey': 'partitionKey'}}
])
def test_apply_retry_logic_on_faulty_lambda_function(record):
    # Setup mocked streams
    sns = boto3.client('sns', region_name='us-east-1')
    sqs = boto3.client('sqs', region_name='us-east-1')
    topic = sns.create_topic(Name=CHANNEL_NAME)
    queue = sqs.create_queue(QueueName=CHANNEL_NAME)
    sns.subscribe(
        TopicArn=topic['TopicArn'],
        Protocol='sqs',
        Endpoint=f'arn:aws:sqs:us-east-1:123456789012:{CHANNEL_NAME}'
    )
    kinesis = boto3.client('kinesis', region_name='us-east-1')
    kinesis.create_stream(StreamName=CHANNEL_NAME, ShardCount=1)

    # Setup lambda environment
    environment = {
        'kinesis_event_source_arn': KINESIS_ARN,
        'sns_event_source_arn': SNS_ARN
    }
    event = {'Records': [record]}
    context = {}

    # Test
    result = apply_retry_logic(environment)(
        faulty_lambda_function)(event, context)
    assert result == FAULT_RESULT


def test_faulty_apply_retry_logic():
    environment = {}
    event = {}
    context = {}
    with pytest.raises(Exception):
        apply_retry_logic(environment)(
            faulty_lambda_function)(event, context)


@pytest.mark.parametrize('event, environment', [
    ({'Records': [{'kinesis': 'kinesis'}]}, {
        'kinesis_event_source_arn': KINESIS_ARN}),
    ({'Records': [{'Sns': 'Sns'}]}, {'sns_event_source_arn': SNS_ARN}),
])
def test_normal_get_stream_parameters(event, environment):
    result = _get_stream_parameters(event, environment)
    assert result[0] in event['Records'][0].keys()
    assert all(map(lambda value: result[1] in value, environment.values()))


@pytest.mark.parametrize('event, environment', [
    ({'Garbage': None}, {}),
    ({'Records': None}, {}),
    ({'Records': [{'some_service': None}]}, {}),
    ({'Records': [{'kinesis': {}}]}, {'ghost_env_var': 'ghost_env_var'}),
    ({'Records': [{'Sns': {}}]}, {'ghost_env_var': 'ghost_env_var'}),
    ({'Records': [{'kinesis': {}}]}, {'kinesis_event_source_arn': None}),
    ({'Records': [{'Sns': {}}]}, {'sns_event_source_arn': None}),
    ({'Records': [{'Ether': {}}]}, None)
])
def test_faulty_get_stream_parameters(event, environment):
    print(event, environment)
    with pytest.raises(Exception):
        _get_stream_parameters(event, environment)


@pytest.mark.parametrize('service, retry_count, record, expect', [
    [
        'kinesis', RETRY_COUNT,
        {
            'data': b64encode(json.dumps({'data': {}}).encode()).decode(),
            'partitionKey': 'partitionKey'
        },
        {
            'Data': '{"data": {}, "retry_attempt": 2}',
            'PartitionKey': "partitionKey"
        }
    ],
    [
        'kinesis', RETRY_COUNT,
        {
            'data': b64encode(json.dumps({'data': {}, 'retry_attempt': 2}).encode()).decode(),
            'partitionKey': 'partitionKey'
        },
        {
            'Data': '{"data": {}, "retry_attempt": 3}',
            'PartitionKey': "partitionKey"
        }
    ],
    [
        'kinesis', 0,
        {
            'data': b64encode(json.dumps({'data': {}, 'retry_attempt': 1}).encode()).decode(),
            'partitionKey': 'partitionKey'
        },
        None
    ],
    ['Sns', RETRY_COUNT, {'Message': {'data': {}}},
        {'data': {}, 'retry_attempt': 2}],
    ['Sns', RETRY_COUNT, {'Message': '{"data": {}}'},
        {'data': {}, 'retry_attempt': 2}],
    ['Sns', RETRY_COUNT, {'Message': {'data': {}, 'retry_attempt': 2}},
        {'data': {}, 'retry_attempt': 3}],
    ['Sns', 0, {'Message': {'data': {}, 'retry_attempt': 1}}, None],
])
def test_normal_resolve_record_retry(service, retry_count, record, expect):
    result = _resolve_record_retry(service, record, retry_count)
    assert result == expect


@pytest.mark.parametrize('service, retry_count, record', [
    ['some_service', None, None],
    ['kinesis', None, {}],
    ['kinesis', None, {'data': None}],
    ['kinesis', None, {'data': ''}],
    ['kinesis', None, {'data': '1'}],
    ['kinesis', None, {'data': '{}'}],
    ['kinesis', None, {'data': '{"data": {}}'}],
    ['Sns', None, {}],
    ['Sns', None, {'Message': None}],
    ['Sns', None, {'Message': []}],
    ['Sns', None, {'Message': {}}],
])
def test_faulty_resolve_record_retry(service, retry_count, record):
    with pytest.raises(Exception):
        _resolve_record_retry(service, record, retry_count)


@mock_sns
@mock_sqs
@mock_kinesis
@pytest.mark.parametrize('service, messages', [
    ['kinesis', [{'Data': 'Data', 'PartitionKey': 'PartitionKey'}]],
    ['Sns', [{}]]
])
def test_normal_put_event_messages(service, messages):
    # Setup mocked streams
    sns = boto3.client('sns', region_name='us-east-1')
    sqs = boto3.client('sqs', region_name='us-east-1')
    topic = sns.create_topic(Name=CHANNEL_NAME)
    queue = sqs.create_queue(QueueName=CHANNEL_NAME)
    sns.subscribe(
        TopicArn=topic['TopicArn'],
        Protocol='sqs',
        Endpoint=f'arn:aws:sqs:us-east-1:123456789012:{CHANNEL_NAME}'
    )
    kinesis = boto3.client('kinesis', region_name='us-east-1')
    kinesis.create_stream(StreamName=CHANNEL_NAME, ShardCount=1)

    # Test
    _put_event_messages(service, CHANNEL_NAME, messages)


@pytest.mark.parametrize('service', [None, 'some_service'])
def test_faulty_put_event_messages(service):
    with pytest.raises(Exception):
        _put_event_messages(service, CHANNEL_NAME, [])
