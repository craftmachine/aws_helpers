from aws_helpers.get_domain import get_domain


def test_get_domain():
    assert get_domain('http://www.craft.co') == 'craft.co'
    assert get_domain('http://craft.co') == 'craft.co'
    assert get_domain('www.craft.co') == 'craft.co'
    assert get_domain('www.craft.co/') == 'craft.co'
    assert get_domain('craft.co') == 'craft.co'
    assert get_domain('http://www.craft.co/test') == 'craft.co'
    assert get_domain('http://www.craft.co/index.php') == 'craft.co'
    assert get_domain('http://www.craft.co/test/test1') == 'craft.co'
    assert get_domain('http://www.craft.co/test?abs') == 'craft.co'
    assert get_domain('http://www.craft.co?test_param=1') == 'craft.co'
    assert get_domain('https://www.craft.co') == 'craft.co'
    assert get_domain('https://www.craft.co/test?abs=2') == 'craft.co'
    assert get_domain('https://craft.co/test?abs=2') == 'craft.co'
    assert get_domain('https://www.en.craft.co/test?abs=2') == 'en.craft.co'
    assert get_domain('https://www.craft.co.uk/test?abs=2') == 'craft.co.uk'
    assert get_domain('https://www.craft.co.test.test/test?abs=2') == 'craft.co.test.test'
    assert get_domain('https://www.craft.co.test.test//') == 'craft.co.test.test'
    assert get_domain('https://www.craft.co.test.test//#') == 'craft.co.test.test'
    assert get_domain('//www.craft.co.test.test//#') == 'craft.co.test.test'
    assert get_domain('//www.craft.co.test.test?abs=2') == 'craft.co.test.test'
    assert get_domain('http://www.http-com.com/') == 'http-com.com'
    assert get_domain('http-com.com/') == 'http-com.com'
    assert get_domain('https://www.royalhaskoningdhv.com:443/') == 'royalhaskoningdhv.com'
    assert get_domain('https://www.royalhaskoningdhv.com:443') == 'royalhaskoningdhv.com'
    assert get_domain('http://www.royalhaskoningdhv.com:443/') == 'royalhaskoningdhv.com'
    assert get_domain('//royalhaskoningdhv.co.uk:443/') == 'royalhaskoningdhv.co.uk'
    assert get_domain('http://royalhaskoningdhv.co.uk:443/') == 'royalhaskoningdhv.co.uk'
    assert get_domain('royalhaskoningdhv.co.uk:80/') == 'royalhaskoningdhv.co.uk'
    assert get_domain('royalhaskoningdhv.co.uk:80/index.php') == 'royalhaskoningdhv.co.uk'
    assert get_domain('royalhaskoningdhv.co.uk:443/test') == 'royalhaskoningdhv.co.uk'
    assert get_domain('https://www.royalhaskoningdhv.co.uk:80?get_params=1') \
        == 'royalhaskoningdhv.co.uk'
    assert get_domain('http://WWW.ACALBFI.FR') == 'acalbfi.fr'
    assert get_domain('HTTP://www.acalbfi.fr') == 'acalbfi.fr'


def test_get_domain_with_subdomain():
    assert get_domain('http://www.craft.co', remove_www=False) == 'www.craft.co'
    assert get_domain('http://craft.co', remove_www=False) == 'craft.co'
    assert get_domain('www.craft.co', remove_www=False) == 'www.craft.co'
    assert get_domain('www.craft.co/', remove_www=False) == 'www.craft.co'
    assert get_domain('craft.co', remove_www=False) == 'craft.co'
    assert get_domain('http://www.craft.co/test', remove_www=False) == 'www.craft.co'
    assert get_domain('http://www.craft.co/index.php', remove_www=False) == 'www.craft.co'
    assert get_domain('http://www.craft.co/test/test1', remove_www=False) == 'www.craft.co'
    assert get_domain('http://www.craft.co/test?abs', remove_www=False) == 'www.craft.co'
    assert get_domain('http://www.craft.co?test_param=1', remove_www=False) == 'www.craft.co'
    assert get_domain('https://www.craft.co', remove_www=False) == 'www.craft.co'
    assert get_domain('https://www.craft.co/test?abs=2', remove_www=False) == 'www.craft.co'
    assert get_domain('https://craft.co/test?abs=2', remove_www=False) == 'craft.co'
    assert get_domain('https://www.en.craft.co/test?abs=2', remove_www=False)\
        == 'www.en.craft.co'
    assert get_domain('https://www.craft.co.uk/test?abs=2', remove_www=False)\
        == 'www.craft.co.uk'
    assert get_domain('https://www.craft.co.test.test/test?abs=2', remove_www=False)\
        == 'www.craft.co.test.test'
    assert get_domain('https://www.craft.co.test.test//', remove_www=False)\
        == 'www.craft.co.test.test'
    assert get_domain('https://www.craft.co.test.test//#', remove_www=False)\
        == 'www.craft.co.test.test'
    assert get_domain('//www.craft.co.test.test//#', remove_www=False)\
        == 'www.craft.co.test.test'
    assert get_domain('//www.craft.co.test.test?abs=2', remove_www=False)\
        == 'www.craft.co.test.test'
    assert get_domain('http-com.com/', remove_www=False)\
        == 'http-com.com'
    assert get_domain('https://www.royalhaskoningdhv.com:80/', remove_www=False)\
        == 'www.royalhaskoningdhv.com'
    assert get_domain('https://www.royalhaskoningdhv.com:443', remove_www=False)\
        == 'www.royalhaskoningdhv.com'
    assert get_domain('http://www.royalhaskoningdhv.com:80/', remove_www=False)\
        == 'www.royalhaskoningdhv.com'
    assert get_domain('//royalhaskoningdhv.co.uk:443/', remove_www=False)\
        == 'royalhaskoningdhv.co.uk'
    assert get_domain('http://royalhaskoningdhv.co.uk:443/', remove_www=False)\
        == 'royalhaskoningdhv.co.uk'
    assert get_domain('royalhaskoningdhv.co.uk:443/', remove_www=False)\
        == 'royalhaskoningdhv.co.uk'
    assert get_domain('www.royalhaskoningdhv.co.uk:443/index.php', remove_www=False)\
        == 'www.royalhaskoningdhv.co.uk'
    assert get_domain('royalhaskoningdhv.co.uk:443/test', remove_www=False)\
        == 'royalhaskoningdhv.co.uk'
    assert get_domain('https://www.royalhaskoningdhv.co.uk:443?get_params=1', remove_www=False) \
        == 'www.royalhaskoningdhv.co.uk'
