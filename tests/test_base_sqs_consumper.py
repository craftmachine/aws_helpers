from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from unittest.mock import patch

import boto3
from moto import mock_cloudwatch, mock_sqs

from aws_helpers.base_sqs_consumer import BaseMessage, BaseSQSConsumer
from aws_helpers.message_bus import put_messages_to_sqs


def test_recieve_messages(access_key, secret_key, region, sqs, queue):
    messages = range(10)
    put_messages_to_sqs(sqs, queue['QueueUrl'], messages)
    with patch('aws_helpers.base_sqs_consumer.BaseSQSConsumer._handle_message') as handle_mock,\
            patch('aws_helpers.base_sqs_consumer.BaseSQSConsumer._build_message') as build_msg_mock:
        sqs_consumer = BaseSQSConsumer(queue['QueueUrl'], access_key, secret_key,
                                       region, 'test', 'test', 5)
        sqs_consumer._recieve_messages()
        for msg in messages:
            build_msg_mock.assert_any_call(msg)
            assert handle_mock.called


def test_recieve_messages_exception_handling(access_key, secret_key, region, sqs, queue):
    messages = ['Test Exception Message']
    put_messages_to_sqs(sqs, queue['QueueUrl'], messages)
    with patch('aws_helpers.base_sqs_consumer.BaseSQSConsumer._handle_message',
               side_effect=Exception('Test')) as handle_mock, \
        patch('aws_helpers.base_sqs_consumer.BaseSQSConsumer._build_message'),\
        patch('aws_helpers.base_sqs_consumer.BaseSQSConsumer._push_cloudwatch_metric') \
            as cloudwatch_mock:
        sqs_consumer = BaseSQSConsumer(queue['QueueUrl'], access_key, secret_key,
                                       region, 'test', 'test', 5)
        sqs_consumer._recieve_messages()
        handle_mock.assert_called_once()
        assert cloudwatch_mock.called


def test_push_cloudwatch_metric(access_key, secret_key, region, sqs, queue, cloudwatch):
    sqs_consumer = BaseSQSConsumer(queue['QueueUrl'], access_key, secret_key,
                                   region, 'test_function', 'test_pipeline', 5)
    sqs_consumer._push_cloudwatch_metric('test_metric', BaseMessage())
    result = _get_cloudwatch_metric(cloudwatch, 'test_metric',
                                    'test_function', 'test_pipeline')
    assert result == 1


def test_push_cloudwatch_metric_concurrently(access_key, secret_key, region,
                                             sqs, queue, cloudwatch):
    sqs_consumer = BaseSQSConsumer(queue['QueueUrl'], access_key, secret_key,
                                   region, 'test_function', 'test_pipeline', 5)
    with ThreadPoolExecutor(2) as ex:
        ex.map(sqs_consumer._push_cloudwatch_metric, ['test_metric', 'test_metric'],
               [BaseMessage(), BaseMessage()], [1, 2])
    result = _get_cloudwatch_metric(cloudwatch, 'test_metric',
                                    'test_function', 'test_pipeline')
    assert result == 3


def _get_cloudwatch_metric(cloudwatch_client, metric_name, function_name, pipeline_name):
    response = cloudwatch_client.get_metric_statistics(
        Namespace='data_pipelines',
        MetricName=metric_name,
        Dimensions=[
            {
                'Name': 'pipeline_name',
                'Value': pipeline_name
            },
            {
                'Name': 'function_name',
                'Value': function_name
            }
        ],
        StartTime=datetime(2018, 1, 1),
        EndTime=datetime.now(),
        Period=60,
        Statistics=['Sum']
    )
    return response['Datapoints'][0]['Sum']
