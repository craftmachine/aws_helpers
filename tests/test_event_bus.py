import json
from unittest.mock import Mock, patch

import boto3
import confluent_kafka
import pytest
from moto import mock_kinesis

from aws_helpers.event_bus import (AUTH_TYPE_CLOUDKARAFKA_SASL,
                                   EventBusException, _kafka_producer_callback,
                                   kafka_consumer, kafka_producer,
                                   process_message, put_message)

EVENT_BUS_STREAM = 'event_bus_stream'
EVENT_VERSION = '1.0'


@mock_kinesis
def test_put_messages_to_kinesis_default_params():
    body = {'company': 'test'}
    kinesis = boto3.client('kinesis', region_name='us-east-1')
    kinesis.create_stream(StreamName=EVENT_BUS_STREAM, ShardCount=1)
    event_type = 'test_event'
    result = put_message(event_type, kinesis_client=kinesis, test=body)
    records = _get_all_records_in_kinesis(kinesis, EVENT_BUS_STREAM)
    kinesis_record = json.loads(records[0]['Data'].decode('utf-8'))
    assert kinesis_record['test'] == body
    assert kinesis_record['type'] == event_type
    assert kinesis_record['version'] == EVENT_VERSION


@mock_kinesis
def test_put_messages_to_kinesis_custom_stream():
    body = {'company': 'test'}
    custom_stream_name = 'test'
    kinesis = boto3.client('kinesis', region_name='us-east-1')
    kinesis.create_stream(StreamName=custom_stream_name, ShardCount=1)
    event_type = 'test_event'
    put_message(event_type,
                kinesis_client=kinesis, stream_name=custom_stream_name, test=body)
    records = _get_all_records_in_kinesis(kinesis, custom_stream_name)
    kinesis_record = json.loads(records[0]['Data'].decode('utf-8'))
    assert kinesis_record['test'] == body
    assert kinesis_record['type'] == event_type
    assert kinesis_record['version'] == EVENT_VERSION


@mock_kinesis
def test_put_messages_to_kinesis_custom_event_version():
    body = {'company': 'test'}
    kinesis = boto3.client('kinesis', region_name='us-east-1')
    kinesis.create_stream(StreamName=EVENT_BUS_STREAM, ShardCount=1)
    event_type = 'test_event'
    event_version = '2.0'
    put_message(event_type, event_version=event_version, kinesis_client=kinesis, test=body)
    records = _get_all_records_in_kinesis(kinesis, EVENT_BUS_STREAM)
    kinesis_record = json.loads(records[0]['Data'].decode('utf-8'))
    assert kinesis_record['test'] == body
    assert kinesis_record['type'] == event_type
    assert kinesis_record['version'] == event_version


@mock_kinesis
def test_put_messages_to_kinesis_fails_if_many_keys():
    body = {'company': 'test'}
    bad_body = 'bad'
    kinesis = boto3.client('kinesis', region_name='us-east-1')
    kinesis.create_stream(StreamName=EVENT_BUS_STREAM, ShardCount=1)
    event_type = 'test_event'
    with pytest.raises(AssertionError):
        put_message(event_type, kinesis_client=kinesis,  test=body, bad=bad_body)


def _get_all_records_in_kinesis(client, stream_name):
    iterator_query_dict = {
        'StreamName': stream_name,
        'ShardIteratorType': 'TRIM_HORIZON',
        'ShardId': 'shardId-000000000000'
    }
    iterator = client.get_shard_iterator(
        **iterator_query_dict)['ShardIterator']
    return client.get_records(ShardIterator=iterator)['Records']


def test_put_message_to_kafka():
    kafka_producer = Mock()
    topic_name = 'event_bus_topic'
    body = {'test': True}
    put_message('event_type', key='test-key', kafka_producer=kafka_producer,
                topic_name=topic_name, test=body)

    assert kafka_producer.produce.called
    args, _ = kafka_producer.produce.call_args
    assert args[0] == topic_name
    event = json.loads(args[1])
    assert event['event_id'] is not None
    assert event['test'] == body

    assert kafka_producer.flush.called


def test_put_message_to_kafka_error():
    kafka_producer = Mock()
    topic_name = 'event_bus_topic'
    body = {'test': True}

    def call_callback():
        _kafka_producer_callback(
            'Something went wrong',
            Mock(value=Mock(return_value='{"event_id": "10f6af4d-7f5c-4a1d-8c86-08289387bf97"}')))
    kafka_producer.flush.side_effect = call_callback

    with pytest.raises(EventBusException):
        put_message('event_type', kafka_producer=kafka_producer, topic_name=topic_name, test=body)


def test_kafka_producer():
    with patch('event_bus.confluent_kafka.Producer', spec=True) as ProducerMock:
        with kafka_producer('client_id', 'example.com'):
            assert ProducerMock.called
            _, kwargs = ProducerMock.call_args
            assert 'sasl.username' not in kwargs
            producer_instance = ProducerMock.return_value
        assert producer_instance.flush.called


def test_kafka_producer_cloudkarafka_sasl():
    with patch('event_bus.confluent_kafka.Producer', spec=True) as ProducerMock:
        with kafka_producer('client_id', 'example.com', 'username', 'password',
                            auth_type=AUTH_TYPE_CLOUDKARAFKA_SASL):
            assert ProducerMock.called
            _, kwargs = ProducerMock.call_args
            assert kwargs['sasl.username'] == 'username'
            producer_instance = ProducerMock.return_value
        assert producer_instance.flush.called


def test_kafka_consumer():
    with patch('event_bus.confluent_kafka.Consumer', spec=True) as ConsumerMock:
        with kafka_consumer('group_id', 'example.com'):
            assert ConsumerMock.called
            _, kwargs = ConsumerMock.call_args
            assert kwargs['client.id'] is not None
            assert 'sasl.username' not in kwargs
            consumer_instance = ConsumerMock.return_value
        assert consumer_instance.close.called


def test_kafka_consumer_cloudkarafka_sasl():
    with patch('event_bus.confluent_kafka.Consumer', spec=True) as ConsumerMock:
        with kafka_consumer('group_id', 'example.com', 'username', 'password',
                            auth_type=AUTH_TYPE_CLOUDKARAFKA_SASL):
            assert ConsumerMock.called
            _, kwargs = ConsumerMock.call_args
            assert kwargs['client.id'] is not None
            assert kwargs['sasl.username'] == 'username'
            consumer_instance = ConsumerMock.return_value
        assert consumer_instance.close.called


def test_process_message_error():
    consumer = Mock(spec=confluent_kafka.Consumer)
    consumer.poll.return_value = Mock(**{'error.return_value': Mock()})
    processor = Mock()
    process_message(consumer, processor)
    assert consumer.poll.called
    assert not processor.called
    assert consumer.commit.called


def test_process_message_partition_end():
    consumer = Mock(spec=confluent_kafka.Consumer)
    msg = Mock(**{
        'error.return_value': Mock(**{
            'code.return_value': confluent_kafka.KafkaError._PARTITION_EOF
        })
    })
    consumer.poll.return_value = msg
    processor = Mock()
    process_message(consumer, processor)
    assert consumer.poll.called
    assert not processor.called
    assert consumer.commit.called
    assert msg.error.return_value.code.called
    assert msg.partition.called


def test_process_message():
    consumer = Mock(spec=confluent_kafka.Consumer)
    msg = Mock(**{
        'value.return_value': '{"test": 1}',
        'error.return_value': None,
        'timestamp.return_value': (None, 100000000)
    })
    consumer.poll.return_value = msg
    processor = Mock()
    process_message(consumer, processor)
    assert consumer.poll.called
    assert processor.called
    assert consumer.commit.called


def test_process_message_processor_error():
    consumer = Mock(spec=confluent_kafka.Consumer)
    msg = Mock(**{
        'value.return_value': '{"test": 1}',
        'error.return_value': None,
        'timestamp.return_value': (None, 100000000)
    })
    consumer.poll.return_value = msg
    processor = Mock(side_effect=ValueError)
    with pytest.raises(ValueError):
        process_message(consumer, processor)
    assert consumer.poll.called
    assert processor.called
    assert not consumer.commit.called
