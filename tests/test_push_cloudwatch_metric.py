from datetime import datetime

import boto3
from moto import mock_cloudwatch

from aws_helpers.push_cloudwatch_metric import push_cloudwatch_metric


@mock_cloudwatch
def test_push_cloudwatch_metric():
    cloudwatch_client = boto3.client('cloudwatch', region_name='us-east-1')
    push_cloudwatch_metric(cloudwatch_client, 'test_metric', 'test_function', 'test_pipeline')
    result = _get_cloudwatch_metric(cloudwatch_client, 'test_metric',
                                    'test_function', 'test_pipeline')
    assert result == 1


@mock_cloudwatch
def test_push_custom_value_to_cloudwatch_metric():
    cloudwatch_client = boto3.client('cloudwatch', region_name='us-east-1')
    custom_value = 10
    push_cloudwatch_metric(cloudwatch_client, 'test_metric', 'test_function',
                           'test_pipeline', value=custom_value)
    result = _get_cloudwatch_metric(cloudwatch_client, 'test_metric',
                                    'test_function', 'test_pipeline')
    assert result == custom_value


def _get_cloudwatch_metric(cloudwatch_client, metric_name, function_name, pipeline_name):
    response = cloudwatch_client.get_metric_statistics(
        Namespace='data_pipelines',
        MetricName=metric_name,
        Dimensions=[
            {
                'Name': 'pipeline_name',
                'Value': pipeline_name
            },
            {
                'Name': 'function_name',
                'Value': function_name
            }
        ],
        StartTime=datetime(2018, 1, 1),
        EndTime=datetime.now(),
        Period=60,
        Statistics=['Sum']
    )
    return response['Datapoints'][0]['Sum']
