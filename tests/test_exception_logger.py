from aws_helpers.exception_logger import ExceptionLogger


@ExceptionLogger(max_attempt_number=3, max_delay=5)
def f(a, b):
    return a + b


def test_exception_logger_if_exception():
    event = {'Records': [1, None]}
    result = f(event, 1)
    assert isinstance(result, list)


def test_exception_logger_if_timeout():
    event = {'Records': [i for i in range(10*20)]}
    result = f(event, 1)
    assert isinstance(result, list)


def test_exception_logger():
    event = {'Records': [1]}
    result = f(event, 1)
    assert isinstance(result, list)
    assert result[0] == 2
