import json
import pickle
import sys

import pytest

from aws_helpers.lib import get_chunks, get_size


def test_get_chunks():
    chunk_size = 50
    test_list = list(range(9999))
    result_list = list(get_chunks(test_list, chunk_size))
    last_chunk = result_list.pop()
    assert len(last_chunk) <= chunk_size
    for chunk in result_list:
        assert len(chunk) == chunk_size
    result_list.append(last_chunk)
    result_list_joined = []
    for chunk in result_list:
        result_list_joined.extend(chunk)
    assert result_list_joined == test_list


@pytest.mark.parametrize('obj', [int(), str(), tuple(), list(), dict()])
def test_get_size(obj):
    expect_json = sys.getsizeof(json.dumps(obj))
    expect_bytes = sys.getsizeof(pickle.dumps(obj))
    assert expect_json == get_size(obj)
    assert expect_bytes == get_size(obj, of_json=False)
