import re
from urllib.parse import urlparse


def get_domain(uri: str, remove_www: bool=True):
    uri = uri.lower()
    uri = re.sub(r':\d+', '', uri)
    parsed_uri = urlparse(uri)
    if uri.startswith('http:') or uri.startswith('https:') or uri.startswith('//'):
        domain = parsed_uri.netloc
    else:
        domain = parsed_uri.path
    if remove_www:
        domain = domain.split('www.')[-1]
    domain = domain.split('/')[0]
    return domain
