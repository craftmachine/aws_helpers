import asyncio
import logging
import time


class ExceptionLogger(object):
    def __init__(self, max_attempt_number=3, max_delay=25):
        self.max_attempt_number = max_attempt_number
        self.min_delay = 1
        self.max_delay = max_delay
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

    def __call__(self, function):
        def wrapped_f(event, context):
            events = self._parse_events(event)
            cap = self._get_cap(len(events))

            ioloop = asyncio.get_event_loop()
            tasks = [self.create_task(cap, event, context, function) for event in events]
            wait_tasks = asyncio.wait(tasks)
            return self._get_results(ioloop.run_until_complete(wait_tasks))
        return wrapped_f

    async def create_task(self, cap, event, context, function):
        start_time = float(time.time())
        delay_since_first_run = float(time.time()) - start_time
        current_attempt_number, current_delay = 0, 0
        exception_message = None
        delay = self.min_delay

        while current_attempt_number < self.max_attempt_number and\
                delay_since_first_run < self.max_delay:
            try:
                return function(event, context)
            except Exception as e:
                await asyncio.sleep(delay)
                current_delay += delay
                delay = min(cap, delay * 2 ** self.max_attempt_number)
                current_attempt_number += 1
                exception_message = e
            delay_since_first_run = float(time.time()) - start_time

        if exception_message:
            self.logger.error('''Failed to run lambda function: {} {}. Input event: {}'''
                              .format(type(exception_message).__name__, exception_message, event))

        if delay_since_first_run >= self.max_delay:
            self.logger.error('Timeout lambda function')

    def _get_results(self, results):
        ans = []
        for task in results[0]:
            ans.append(task.result())
        return ans

    def _get_cap(self, count_events):
        return (self.max_delay + self.min_delay) / (count_events)

    def _parse_events(self, event):
        if isinstance(event, dict):
            if 'Records' in event.keys():
                return event['Records']
        return [event]
