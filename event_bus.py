"""
This module includes functionality to interact with event bus.

Kafka consumer example:

    def processor(msg, arrival_time):
        print(f'msg={msg} arrival_time={arrival_time}')

    with event_bus.kafka_consumer('your-consumer-group', brokers) as consumer:
        consumer.subscribe(['your-awesome-topic'])
        while True:
            event_bus.process_message(consumer, processor)

Kafka producer example:

    with event_bus.kafka_producer('your-client-id', brokers) as producer:
        event_bus.put_message(type='event_type',
                              kafka_producer=producer, topic_name='your-topic',
                              key='company-123', domain_related_stuff={})

Both event_bus.kafka_producer and event_bus.kafka_consumer support Cloudkarafka's
authentication from outside of peered VPC:

    with event_bus.kafka_producer('your-client-id', public_broker_addresses,
                                  username, password,
                                  auth_type=AUTH_TYPE_CLOUDKARAFKA_SASL) as producer:
        # your code
"""

import json
import logging
import random
import string
import time
from contextlib import contextmanager
from datetime import datetime, timezone
from pathlib import Path
from threading import Lock
from typing import Callable
from uuid import uuid4

LOGGER = logging.getLogger(__name__)
EVENT_BUS_STREAM_NAME = 'event_bus_stream'
EVENT_VERSION = '1.0'
AUTH_TYPE_CLOUDKARAFKA_SASL = 'cloudkarafka_sasl'
AUTH_TYPE_NONE = 'none'
_KAFKA_PRODUCER_LOCK = Lock()
_KAFKA_PRODUCER_LOCK_TIMEOUT_SECS = 10
_KAFKA_CA_CERT_PATH = f'{Path(__file__).parent}/cloudkarafka.ca'
_KAFKA_POLL_TIMEOUT = 1.0

try:
    import confluent_kafka
except ImportError:
    # support old Kinesis-only users
    LOGGER.error('Failed to load confluent_kafka')


def build_kafka_producer(client_id, bootstrap_servers,
                         username=None, password=None,
                         auth_type=AUTH_TYPE_NONE,
                         **kwargs):
    conf = {
        'client.id': client_id,
        'bootstrap.servers': bootstrap_servers,
        'session.timeout.ms': 6000,
        'default.topic.config': {'auto.offset.reset': 'smallest'},
    }
    if auth_type == AUTH_TYPE_CLOUDKARAFKA_SASL:
        conf.update(_get_cloudkarafka_sasl_config(username, password))
    conf.update(kwargs)
    return confluent_kafka.Producer(**conf)


@contextmanager
def kafka_producer(client_id, bootstrap_servers,
                   username=None, password=None,
                   auth_type=AUTH_TYPE_NONE, **kwargs):
    producer = build_kafka_producer(client_id, bootstrap_servers,
                                    username, password,
                                    auth_type=auth_type, **kwargs)
    try:
        yield producer
    finally:
        producer.flush()


def build_kafka_consumer(group_id, bootstrap_servers,
                         username=None, password=None,
                         auth_type=AUTH_TYPE_NONE, **kwargs):
    conf = {
        'bootstrap.servers': bootstrap_servers,
        'group.id': group_id,
        'session.timeout.ms': 6000,
        'default.topic.config': {'auto.offset.reset': 'smallest'},
        'enable.auto.commit': False
    }
    if auth_type == AUTH_TYPE_CLOUDKARAFKA_SASL:
        conf.update(_get_cloudkarafka_sasl_config(username, password))
    conf.update(kwargs)

    if 'client.id' not in conf:
        # generate random client ID if it isn't given
        conf['client.id'] = ''.join(
            random.choices(string.ascii_uppercase + string.digits, k=6)
        )

    client_id = conf['client.id']
    LOGGER.info(f'Creating Kafka consumer client_id={client_id}')

    return confluent_kafka.Consumer(**conf)


@contextmanager
def kafka_consumer(group_id, bootstrap_servers,
                   username=None, password=None,
                   auth_type=AUTH_TYPE_NONE, **kwargs):
    consumer = build_kafka_consumer(group_id, bootstrap_servers,
                                    username, password,
                                    auth_type=auth_type, **kwargs)
    try:
        yield consumer
    finally:
        consumer.close()


def _get_cloudkarafka_sasl_config(username, password):
    return {
        'security.protocol': 'SASL_SSL',
        'sasl.mechanisms': 'SCRAM-SHA-256',
        'ssl.ca.location': _KAFKA_CA_CERT_PATH,
        'sasl.username': username,
        'sasl.password': password
    }


def put_message(type: str, event_version: str = EVENT_VERSION,
                kinesis_client=None, kafka_producer=None,
                stream_name: str = EVENT_BUS_STREAM_NAME, topic_name: str = None,
                key=None,
                **domain_data):
    """Post a message to event bus. It supports Kinesis and Kafka and, depending
    on arguments, the message will be posted to both or one of them.

    If you use Kafka, use the key argument - it'll guarantee the ordering of events with the same
    key which could be very useful for event sourcing. For instance, for company_changed events,
    the key should be a string in the format "company-{cid}".

    The event format is documented at
    https://app.nuclino.com/Craft/Engineering/Events-48c0e9d8-0f9e-4c7c-9d44-389d544b45a1.
    """
    assert len(domain_data) == 1
    assert kinesis_client is not None or kafka_producer is not None

    event = {
        'event_id': str(uuid4()),
        'type': type,
        'version': event_version,
        'timestamp': int(time.time()),
    }
    for k, v in domain_data.items():
        event[k] = v

    LOGGER.info(f'Posting event to event bus event_id={event["event_id"]}')

    if kinesis_client is not None:
        LOGGER.info(f'Posting event to kinesis event_id={event["event_id"]}')
        kinesis_client.put_record(StreamName=stream_name,
                                  PartitionKey=event['event_id'],
                                  Data=json.dumps(event))

    if kafka_producer is not None:
        LOGGER.info(f'Posting event to kafka event_id={event["event_id"]}')

        # lock to get errors in the same thread
        if not _KAFKA_PRODUCER_LOCK.acquire(timeout=_KAFKA_PRODUCER_LOCK_TIMEOUT_SECS):
            raise EventBusException('Failed to acquire lock')
        try:
            kafka_producer.produce(topic_name, json.dumps(event), key,
                                   callback=_kafka_producer_callback)
            # make message sending synchronous
            kafka_producer.flush()
        finally:
            _KAFKA_PRODUCER_LOCK.release()


def process_message(kafka_consumer,
                    processor: Callable[[str, datetime], None],
                    timeout=_KAFKA_POLL_TIMEOUT):
    '''
    Processes the event meessage, `polled` by the `kafka_consumer`,
    using the provided `processor` function, and `commits` the event message
    after its successful processing.

    Returns:
        `True` - kafka event message is polled, processed and commited
        `False` - kafka event message isn't polled due to its unavaiability
                  during `timeout` seconds after polling is started
    '''
    msg = kafka_consumer.poll(timeout=timeout)
    if msg is None:
        return False

    if msg.error():
        if msg.error().code() == confluent_kafka.KafkaError._PARTITION_EOF:
            LOGGER.info(f'Reached end of partition {msg.partition()} at offset {msg.offset()}')
        else:
            LOGGER.error(f'Kafka error: {msg.error()}')
        kafka_consumer.commit()
        return True

    _, timestamp_ms = msg.timestamp()
    event_arrival_time = datetime.utcfromtimestamp(timestamp_ms / 1000).replace(tzinfo=timezone.utc)

    processor(msg.value(), event_arrival_time)

    # commit only if no errors
    kafka_consumer.commit()
    return True


class EventBusException(Exception):
    pass


def _kafka_producer_callback(err, msg):
    event = json.loads(msg.value())
    if err:
        LOGGER.error(f'Event delivery failed event_id={event["event_id"]} topic={msg.topic()} '
                     f'partition={msg.partition()}: {err}')
        raise EventBusException(err)
    else:
        LOGGER.info(f'Event delivery succeeded event_id={event["event_id"]} topic={msg.topic()} '
                    f'partition={msg.partition()}')
