from distutils.core import setup

setup(
    name='aws_helpers',
    version='1.6',
    package_dir={'aws_helpers': ''},
    packages=['aws_helpers'],
    include_package_data=True,
    package_data={'': ['cloudkarafka.ca']}
)
